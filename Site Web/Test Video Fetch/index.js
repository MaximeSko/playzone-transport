function  afficherVideo(){
// Mon url
var url = "http://localhost:51790/api/clipvideo/2";
  // Appelez fetch(url) avec les option par défaut.
  // Retourner la promesse dans une variable.
  var promesse = fetch(url);
  // Travailler avec la promesse:
  promesse
    .then(function(response) {
        console.log("OK! je reçois un objet qui est:");
        console.log(response);
        if(!response.ok) {
           throw new Error("HTTP error, status = " + response.status);
        }
        // Obtenir la promesse de blob:
        var myBlob_Leponge= response.json();
        return myBlob_Leponge;
    })
    .then(function(jsonVideo) {
        console.log("OK! Blob:");
        console.log(jsonVideo);
        console.log(jsonVideo.Lien);
        //Mettre le retour du Blob dans une variable pour l'afficher dans iFrame
         var iframeVideo = document.getElementById("iframePlayer");
         iframeVideo.src = jsonVideo.Lien;
        console.log(jsonVideo.Lien);
    })
    .catch(function(error)  {
        console.log("Une Erreur s'est produite:");
        console.log(error);
         var iframeVideo = document.getElementById("iframePlayer");
         iframeVideo.src ="https://image.flaticon.com/icons/png/512/66/66872.png";
        alert("Erreur inattendue, Veuillez contacter un technicien !");
    }); 
}

